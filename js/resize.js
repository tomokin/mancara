window.onload = function() {
    // const canvas = document.querySelector('#board canvas');
    // const ctx = canvas.getContext('2d');
  
    function fitCanvasSize() {
        // Canvas のサイズをクライアントサイズに合わせる
        canvas.width = document.documentElement.clientWidth - 20;
        canvas.height = document.documentElement.clientHeight - 50;

        unit_block_width = canvas.width / 41;
        unit_block_height = canvas.height / 10;
        unit_length = Math.min(unit_block_width,unit_block_height);

        draw_background();
        draw_player_field();
        draw_score();
    }

    fitCanvasSize();
    window.onresize = fitCanvasSize;
  }