// 色の設定
const backgroud_color = "rgb(244,164,96)";
const cup_color = "rgb(210,105,30)";
const text_font = "px Segoe";
const text_color = "rgb(105,105,105)";
const score_color = "rgb(219,112,147)";
const turn_color = '#00bfff';
//入力をキャッチ
const controls = new KeyControls;
const m_controls = new MouseControls;

const canvas = document.querySelector('#board canvas');
const ctx = canvas.getContext('2d');

//描画の最小単位の設定
//resize.js内で、windowのサイズが変わると追随して変更される
let unit_block_width = canvas.width / 41;
let unit_block_height = canvas.height / 10;
let unit_length = Math.min(unit_block_width, unit_block_height);

//描画するブロックの位置とサイズ
//単位は上記のunit_block
const canvas_each_block_init = [6, 1, 4, 3];
const canvas_each_block_delta = [5, 5];
const canvas_num_each_block = [6, 2];
const canvas_result_blocks = [[1, 1, 4, 7], [36, 2, 4, 7]];

const canvas_turn_blocks = [[0.5, 0.5, 35, 4], [5.5, 5.5, 35, 4]];

const canvas_player_text = [[0.5,0.5],[35,9.75]];

//for文で生成する描画するブロックの位置とサイズ
//ブロックの位置とindexの対応は以下
// 05 04 03 02 01 00
// 06 07 08 09 10 11
//以降のeach_scoreもこのindexに追随する
const canvas_each_blocks = []; 
function make_blocks() {
    let h = canvas_each_block_init[1];
    for (let i = 1; i <= canvas_num_each_block[0]; ++i) {
        let w = canvas_each_block_init[0] + (canvas_num_each_block[0] - i) * canvas_each_block_delta[0];
        canvas_each_blocks.push([w, h, canvas_each_block_init[2], canvas_each_block_init[3]]);
    }
    h = canvas_each_block_init[1] + canvas_each_block_delta[1];
    for (let i = 0; i < canvas_num_each_block[0]; ++i) {
        let w = canvas_each_block_init[0] + i * canvas_each_block_delta[0];
        canvas_each_blocks.push([w, h, canvas_each_block_init[2], canvas_each_block_init[3]]);
    }
}
make_blocks();

let each_score = new Array(canvas_num_each_block[0] * canvas_num_each_block[1]).fill(4);
let result_scores = [0, 0];
function init_score() {
    each_score.fill(4);
    result_scores.fill(0);
}
//trueが0～5, falseが6～11
let is_turn = true;

//おはじきの数を画面中央にまで移動するアニメーション
let anime1 = new Anime1(-1, 0, 1);
//おはじきを各ブロックに分配するアニメーション
//1つのブロックにつき、1つのコンストラクタで対応する
let anime2 = new Anime2(-1, -1, 0, 1);
//移動するおはじきのindex
let target_index = 2;
//anime2において、移動先のブロックのindex
let current_anime_index = 0;
//中央にテキストを表示するアニメーション
let anime3 = new Anime3("", 0, 2);
//横取り用のアニメーション (anime1を使いまわし)
//おはじきの数を画面中央にまで移動する
let anime1_yokodori1 = new Anime1(-1,0,1);
let anime1_yokodori2 = new Anime1(-1,0,1);
//横取り用のアニメーション
//おはじきの数を画面中央からresult blockまで移動する
let anime4 = new Anime4(-1,true,0,1);

let anime_start = false;

let gama_start = false;
//test
//each_score[5] = 0;