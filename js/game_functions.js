function get_index_by_canvas(x, y){
    let return_index = -1;
    let block_x = x / unit_block_width;
    let block_y = y / unit_block_height;
    canvas_each_blocks.forEach(function (elem, index) {
        if( elem[0] < block_x && block_x < elem[0]+elem[2] && elem[1] < block_y && block_y < elem[1]+elem[3]){
            return_index = index;
        }
    });
    if(is_turn && return_index < 6 || !is_turn && 6 <= return_index){
        return return_index;
    }else{
        return -1;
    }
}

function is_finish(){
    let sum_p1 = 0;
    let sum_p2 = 0;
    for(let i=0; i<6;++i){
        sum_p1 += each_score[i];
        sum_p2 += each_score[i+6];
    }
    return (sum_p1 == 0) || (sum_p2 == 0);
}