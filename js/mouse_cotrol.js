
class MouseControls {
    constructor() {
        // クリックした座標を取得
        this.onclick_position = [0,0];
        this.over_position = [0,0];

        document.addEventListener(
            'click',
            e => {
                let rect = e.target.getBoundingClientRect();
                let x = e.clientX - rect.left;
                let y = e.clientY - rect.top;
                this.onclick_position = [x,y];
                console.log("onclick "+ this.onclick_position);
                if(get_index_by_canvas(x,y) >= 0 && gama_start){
                    target_index = get_index_by_canvas(x,y);
                    anime_start = true;
                    console.log(get_index_by_canvas(x,y),x,y);
                }
            },
            false,
        );

        document.addEventListener(
            'mouseover',
            e => {
                let rect = e.target.getBoundingClientRect();
                let x = e.clientX - rect.left;
                let y = e.clientY - rect.top;
                this.over_position = [x,y];
                console.log("mouseover "+ this.over_position);
                canvas.addEventListener('mousemove', 
                e => {
                    let rect = e.target.getBoundingClientRect();
                    let x = e.clientX - rect.left;
                    let y = e.clientY - rect.top;
                    if(get_index_by_canvas(x,y) >= 0){
                        console.log(get_index_by_canvas(x,y),x,y);
                    }
                },
                false);
            },
            false,
        );
    }
};
