class Anime3 {
    constructor(text, unit_block, seconds) {
        this.anime_time = seconds * 60;
        this.valid = true;
        this.counter = 0;

        this.unit_block = unit_block;
        this.text = text;
        if (text !=  "") {
            let len = text.length;
            this.text_position = [canvas.width / 2 - len * unit_block / 2, canvas.height / 2 + unit_block / 2,];
        }else{
            this.valid = false;
        }
    };
    get myvalid() {
        return this.valid;
    }
    get myindex() {
        return this.index;
    }
    set myvalid(v) {
        this.valid = v;
    }
    get isfinish(){
        return (this.counter >= this.anime_time);
    }
    iterate() {
        if (this.counter < this.anime_time) {
            this.counter += 1;
            if (this.counter >= this.anime_time) {
                this.valid = false;
            }
        }
        this.draw();
    }
    draw() {
        if(this.counter < this.anime_time /2){
            ctx.fillStyle = "rgba(105, 105, 105, " + this.counter*2/this.anime_time +")";
        }else{
            ctx.fillStyle = "rgba(105, 105, 105, " + (2.0 - this.counter*2/this.anime_time) +")";
        }
        ctx.font = "" + this.unit_block + "px Segoe";
        ctx.fillText(
            this.text,
            this.text_position[0],
            this.text_position[1]
        );

    }
}