class Anime1 {
    constructor(index, unit_block, seconds) {
        this.anime_time = seconds * 60;
        this.valid = true;
        if (index < 0) {
            this.valid = false;
        }
        this.counter = 0;

        this.index = index;
        this.unit_block = unit_block;
        if (index >= 0) {
            this.circle_position = [
                (canvas_each_blocks[this.index][0] + canvas_each_blocks[this.index][2] / 2) * unit_block_width - unit_block / 2,
                (canvas_each_blocks[this.index][1] + canvas_each_blocks[this.index][3] / 2) * unit_block_height
            ];
            this.text_position = [
                (canvas_each_blocks[this.index][0] + canvas_each_blocks[this.index][2] / 2) * unit_block_width + unit_block / 2,
                (canvas_each_blocks[this.index][1] + canvas_each_blocks[this.index][3] / 2) * unit_block_height + unit_block / 2
            ];

            let circle_end = [canvas.width / 2 - unit_block / 2, canvas.height / 2];
            let text_end = [canvas.width / 2 + unit_block / 2, canvas.height / 2 + unit_block / 2,];

            this.circle_delta = [
                (circle_end[0] - this.circle_position[0]) / this.anime_time,
                (circle_end[1] - this.circle_position[1]) / this.anime_time
            ];
            this.text_delta = [
                (text_end[0] - this.text_position[0]) / this.anime_time,
                (text_end[1] - this.text_position[1]) / this.anime_time
            ];
        }
    };
    get myvalid() {
        return this.valid;
    }
    get myindex() {
        return this.index;
    }
    set myvalid(v) {
        this.valid = v;
    }
    get isfinish(){
        return (this.counter >= this.anime_time);
    }
    iterate() {
        if (this.counter < this.anime_time) {
            this.circle_position[0] += this.circle_delta[0];
            this.text_position[0] += this.text_delta[0];
            this.circle_position[1] += this.circle_delta[1];
            this.text_position[1] += this.text_delta[1];
            this.counter += 1;
            // if (this.counter >= this.anime_time) {
            //     this.valid = false;
            // }
        }
        this.draw();
    }
    draw() {
        ctx.beginPath();
        ctx.fillStyle = score_color;
        ctx.arc(
            this.circle_position[0],
            this.circle_position[1],
            this.unit_block / 2,
            0,
            Math.PI * 2,
            true
        );
        ctx.fill();

        ctx.fillStyle = text_color;
        ctx.font = "" + this.unit_block + "px Segoe";
        let text = "×" + each_score[this.index];
        ctx.fillText(
            text,
            this.text_position[0],
            this.text_position[1]
        );

    }
}