class Anime4 {
    constructor(index, is_turn, unit_block, seconds) {
        this.anime_time = seconds * 60;
        this.valid = true;
        if (index < 0) {
            this.valid = false;
        }
        this.counter = 0;
        this.index = index;
        this.unit_block = unit_block;
        let result_index = 0;
        this.is_turn = is_turn;
        if (!is_turn) {
            result_index = 1;
        }

        if (index >= 0) {
            this.sum_score = each_score[index] + each_score[11-index];

            this.circle_position = [canvas.width / 2 - unit_block / 2, canvas.height / 2];
            this.text_position = [canvas.width / 2 + unit_block / 2, canvas.height / 2 + unit_block / 2];
            let circle_end = [
                (canvas_result_blocks[result_index][0] + canvas_result_blocks[result_index][2] / 2) * unit_block_width - unit_block / 2,
                (canvas_result_blocks[result_index][1] + canvas_result_blocks[result_index][3] / 2) * unit_block_height
            ];
            let text_end = [
                (canvas_result_blocks[result_index][0] + canvas_result_blocks[result_index][2] / 2) * unit_block_width + unit_block / 2,
                (canvas_result_blocks[result_index][1] + canvas_result_blocks[result_index][3] / 2) * unit_block_height + unit_block / 2
            ];

            this.circle_delta = [
                (circle_end[0] - this.circle_position[0]) / this.anime_time,
                (circle_end[1] - this.circle_position[1]) / this.anime_time
            ];
            this.text_delta = [
                (text_end[0] - this.text_position[0]) / this.anime_time,
                (text_end[1] - this.text_position[1]) / this.anime_time
            ];
        }
    };

    get myvalid() {
        return this.valid;
    }
    get myindex() {
        return this.index;
    }
    set myvalid(v) {
        this.valid = v;
    }
    get getresultindex() {
        if (this.is_turn) {
            return 0;
        } else {
            return 1;
        }
    }
    get isfinish(){
        return (this.counter >= this.anime_time);
    }
    iterate() {
        if (this.counter < this.anime_time) {
            this.circle_position[0] += this.circle_delta[0];
            this.text_position[0] += this.text_delta[0];
            this.circle_position[1] += this.circle_delta[1];
            this.text_position[1] += this.text_delta[1];
            this.counter += 1;
            // if (this.counter >= this.anime_time) {
            //     this.valid = false;
            // }
        }
        this.draw();
    }
    draw() {
        ctx.beginPath();
        ctx.fillStyle = score_color;
        ctx.arc(
            this.circle_position[0],
            this.circle_position[1],
            this.unit_block / 2,
            0,
            Math.PI * 2,
            true
        );
        ctx.fill();

        ctx.fillStyle = text_color;
        ctx.font = "" + this.unit_block + "px Segoe";
        let text = "×" + this.sum_score;
        ctx.fillText(
            text,
            this.text_position[0],
            this.text_position[1]
        );

    }}