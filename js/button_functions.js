document.querySelector('#buttons #howtoplay').addEventListener(
    'click',
    e => {
        console.log("onclick howtoplay");
        window.open('howtoplay.html', 'subwin', 'width=600,height=360');
    },
    false,
);

document.querySelector('#buttons #reset').addEventListener(
    'click',
    e => {
        console.log("onclick gamereset");
        init_score();
        if (!anime3.myvalid) {
            anime3 = new Anime3("", unit_length, 10);
        }
        gama_start = true;
        is_turn = true;
        anime3 = new Anime3("Game Start", unit_length, 2);
    },
    false,
);
