function loop() {

  draw_background();
  draw_player_field();
  draw_score();

  // 次のフレイムでも実行したいからメイン関数でも呼び出す
  requestAnimationFrame(loop)
 
  // アクションキー（スペース）を押すと色が変わる
  if (controls.action) {
    // anime_start = true;
    gama_start = true;
  }

  if (anime_start) {
    //test
    current_anime_index = target_index;
    anime1 = new Anime1(target_index, unit_length, 1);
    anime_start = false;
  }

  if (anime1.isfinish) {
    if (!anime2.myvalid) {
      current_anime_index += 1;
      current_anime_index %= 12;
      let is_result_block = (is_turn && current_anime_index == canvas_num_each_block[0])
        || (!is_turn && current_anime_index == 0);

      if (is_result_block) {
        anime2 = new Anime2_result(target_index, is_turn, unit_length, 1);
      } else {
        anime2 = new Anime2(target_index, current_anime_index, unit_length, 1);
      }
    }

    if (anime2.isfinish) {
      each_score[target_index] -= 1;

      let is_result_index = anime2.getresultblock;
      let is_result_block = false;
      if (is_result_index >= 0) {
        result_scores[is_result_index] += 1;
      } else {
        each_score[current_anime_index] += 1;
        current_anime_index += 1;
        current_anime_index %= 12;

        is_result_block = (is_turn && current_anime_index == canvas_num_each_block[0])
          || (!is_turn && current_anime_index == 0);
      }

      if (each_score[target_index] > 0) {
        if (is_result_block) {
          anime2 = new Anime2_result(target_index, is_turn, unit_length, 1);
        } else {
          anime2 = new Anime2(target_index, current_anime_index, unit_length, 1);
        }
      } else { //ここに来ると、アニメーションの連鎖終わり
        if (is_result_index >= 0) {
          anime3 = new Anime3("ピッタリ（もう一度自分のターン！）", unit_length, 2);
        } else {
          let current_index = (current_anime_index + 11) % 12;
          let is_yokodori = (is_turn && current_index < 6 && each_score[current_index] == 1 && each_score[11 - current_index] > 0) ||
            (!is_turn && current_index >= 6 && each_score[current_index] == 1 && each_score[11 - current_index] > 0);
          if (is_yokodori) {
            anime3 = new Anime3("横取り（相手のコマをゲット！）", unit_length, 2);
            anime1_yokodori1 = new Anime1(current_index, unit_length, 2);
            anime1_yokodori2 = new Anime1(11 - current_index, unit_length, 2);
          } else {
            anime3 = new Anime3("ターン交代", unit_length, 2);
            is_turn = !is_turn;
          }
        }

        anime2 = new Anime2(-1, -1, unit_length, 0);
        anime1 = new Anime1(-1, unit_length, 2);

        //test
        target_index += 1;
        target_index %= 12;
      }
    }
  }
  if (anime1_yokodori1.isfinish) {
    if (!anime4.valid) {
      anime4 = new Anime4(anime1_yokodori1.myindex, is_turn, unit_length, 2);
      anime1_yokodori1 = new Anime1(-1, unit_length, 2);
      anime1_yokodori2 = new Anime1(-1, unit_length, 2);
    }
  }
  if (anime4.isfinish) {
    let index = anime4.myindex;
    let result_index = anime4.getresultindex;
    result_scores[result_index] += each_score[index] + each_score[11 - index];
    each_score[index] = 0;
    each_score[11 - index] = 0;
    anime4 = new Anime4(-1, true, 0, 1);
    anime3 = new Anime3("ターン交代", unit_length, 2);
    is_turn = !is_turn;
  }

  if (is_finish()) {
    for (let i = 0; i < 6; ++i) {
      result_scores[0] += each_score[i];
      each_score[i] = 0;
      result_scores[1] += each_score[i + 6];
      each_score[i + 6] = 0;
    }
    let text = "Winner is Player1";
    if (result_scores[0] < result_scores[1]) {
      text = "Winner is Player2";
    } else if (result_scores[0] == result_scores[1]) {
      text = "Draw"
    }
    if(!anime3.myvalid){
      anime3 = new Anime3(text, unit_length, 10);
    }
  }

  if (anime1.myvalid) {
    anime1.iterate();
  }
  if (anime2.myvalid) {
    anime2.iterate();
  }
  if (anime3.myvalid) {
    anime3.iterate();
  }
  if (anime1_yokodori1.myvalid) {
    anime1_yokodori1.iterate();
  }
  if (anime1_yokodori2.myvalid) {
    anime1_yokodori2.iterate();
  }
  if (anime4.myvalid) {
    anime4.iterate();
  }

}

requestAnimationFrame(loop)