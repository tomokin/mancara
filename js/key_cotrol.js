
class KeyControls {
    constructor() {
        // 押したキーを保管するオブジェクト
        this.keys = {};
        // キーを押したら…
        document.addEventListener(
            'keydown',
            e => {
                if ([37, 38, 39, 40].indexOf(e.which) >= 0) {
                    // 矢印押したらスクロールしないように
                    e.preventDefault();
                }
                // 押したキーをtrueにする
                this.keys[e.which] = true;
            },
            false,
        );

        document.addEventListener(
            'keyup',
            e => {
                // 話したキーをfalseにする
                this.keys[e.which] = false;
            },
            false,
        );
    }

    // スペース（キー３２）を押したらtrueを返す
    get action() {
        return this.keys[32];
    }

    get x() {
        // ←、またはAを押されたら −1 返す
        if (this.keys[37] || this.keys[65]) return -1;
        // →またはDを押されたら 1 返す
        if (this.keys[39] || this.keys[68]) return 1;
        return 0;
    }

    get y() {
        //  ↑、またはWを押されたら −1 返す
        if (this.keys[38] || this.keys[87]) return -1;
        //  ↓、またはSを押されたら −1 返す
        if (this.keys[40] || this.keys[83]) return 1;
        return 0;
    }
}

