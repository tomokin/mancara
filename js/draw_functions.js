function draw_background() {
    ctx.fillStyle = backgroud_color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = cup_color;
    canvas_each_blocks.forEach(function (elem) {
        ctx.fillRect(
            elem[0] * unit_block_width,
            elem[1] * unit_block_height,
            elem[2] * unit_block_width,
            elem[3] * unit_block_height);
    });
    canvas_result_blocks.forEach(function (elem) {
        ctx.fillRect(
            elem[0] * unit_block_width,
            elem[1] * unit_block_height,
            elem[2] * unit_block_width,
            elem[3] * unit_block_height);
    });

    ctx.fillStyle = text_color;
    ctx.font = "" + unit_length + "px Segoe";
    let text = "Player1(先手)";
    ctx.fillText(
        text,
        canvas_player_text[0][0] * unit_block_width,
        canvas_player_text[0][1] * unit_block_height);
    text = "Player2(後手)";
    ctx.fillText(
        text,
        canvas_player_text[1][0] * unit_block_width,
        canvas_player_text[1][1] * unit_block_height);

}

function draw_score() {
    canvas_each_blocks.forEach(function (elem, index) {
        let is_write_anime1 = !anime1.myvalid || (anime1.myindex != index);
        let is_write_anime1_y1 = !anime1_yokodori1 || (anime1_yokodori1.myindex != index);
        let is_write_anime1_y2 = !anime1_yokodori2 || (anime1_yokodori2.myindex != index);
        let is_write_anime4 = !anime4.myvalid || ((anime4.myindex != index) && (anime4.myindex != (11 - index)));

        let is_write = is_write_anime1 && is_write_anime1_y1 && is_write_anime1_y2 && is_write_anime4;
        if (is_write && each_score[index] != 0) {
            let unit_length = Math.min(unit_block_width, unit_block_height);

            ctx.beginPath();
            ctx.fillStyle = score_color;
            ctx.arc(
                (elem[0] + elem[2] / 2) * unit_block_width - unit_length / 2,
                (elem[1] + elem[3] / 2) * unit_block_height,
                unit_length / 2,
                0,
                Math.PI * 2,
                true
            );
            ctx.fill();

            ctx.fillStyle = text_color;
            ctx.font = "" + unit_length + "px Segoe";
            let text = "×" + each_score[index];
            ctx.fillText(
                text,
                (elem[0] + elem[2] / 2) * unit_block_width + unit_length / 2,
                (elem[1] + elem[3] / 2) * unit_block_height + unit_length / 2
            );
        }
    });
    canvas_result_blocks.forEach(function (elem, index) {

        let unit_length = Math.min(unit_block_width, unit_block_height);

        ctx.beginPath();
        ctx.fillStyle = score_color;
        ctx.arc(
            (elem[0] + elem[2] / 2) * unit_block_width - unit_length / 2,
            (elem[1] + elem[3] / 2) * unit_block_height,
            unit_length / 2,
            0,
            Math.PI * 2,
            true
        );
        ctx.fill();

        ctx.fillStyle = text_color;
        ctx.font = "" + unit_length + "px Segoe";
        let text = "×" + result_scores[index];
        ctx.fillText(
            text,
            (elem[0] + elem[2] / 2) * unit_block_width + unit_length / 2,
            (elem[1] + elem[3] / 2) * unit_block_height + unit_length / 2
        );
    });
}

function draw_player_field() {
    let random = Math.random() % 3;
    let index = is_turn ? 0 : 1;
    ctx.fillStyle = turn_color;
    ctx.globalAlpha = 0.1;
    ctx.fillRect(
        canvas_turn_blocks[index][0] * unit_block_width,
        canvas_turn_blocks[index][1] * unit_block_height,
        canvas_turn_blocks[index][2] * unit_block_width,
        canvas_turn_blocks[index][3] * unit_block_height);
    ctx.globalAlpha = 1;
}